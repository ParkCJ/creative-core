﻿==========================================
1. Shaders and Materials: 물체가 어떻게 보이는가를 만드는 것 (표면: 색, 반사, 물리 텍스쳐)
2. Lighting: 자연빛, 인공빛, 그림자
3. Animation: 물체에 움직임을 주는 것
4. Visual effects (VFX): 모션효과
5. Cameras: 너의 눈
6. Post-processing: 사진에 필터걸기
7. Audio
8. User interface (UI): 너와 상호작용
==========================================
UI
==========================================
단어
1. intuitive: 직감에 의한, 이해하기 쉬운
==========================================
사용자가 컴퓨터, 또는 컴퓨터 Application과 소통하게 해주는 것 [08.UI1-What is UI 참고]
==========================================
1. UI Design
	Graphical UI (GUI)을 매력적이고, 직감에 맞게 배치하는 것
	좋은 UI는 유저의 시선에서 사라져야 한다. (키보드처럼)
2. What is NOT UI: [08.UI2-What is NOT UI 참고]
	User experience (UX) design
		Answers the question: What’s the user journey?
		Goal: Ensure a positive overall experience with a product from start to finish.

	User interaction design (IxD)
		Answers the question: What does the user need to achieve their goal?
		Goal: Allow users to accomplish their goals in the most efficient way. 
		Is part of UX design.

	User interface (UI) design
		Answers the question: What should the UI elements look like on screen?
		Goal: Make the interface easy to understand and easy to use without getting in the way.
		Is part of IxD, which is part of UX.

	Information architecture
		Answers the question: How should information be organized and structured (not necessarily for interactions)?
		Goal: Structure content so users know where to go to find the information they need.
		Informs UI design, IxD and UX design.

	Visual design
		Answers the question: What will look the best (not necessarily for interactions)?
		Goal: Make something visually appealing.
		Informs UI design, IxD and UX design.

3. UI 개발의 3가지 방법 [08.UI3-3 ways UI developing 참고]
4. Accessibility: 다른 물리적 능력을 가진 사람들이, 얼마나 잘 성공적으로 Application과 소통할 수 있는가
5. TextMeshPro 용 폰트 만들기
	1) 폰트를 다운 받는다. (.otf, .ttf)
	2) 프로젝트에 넣는다.
	3) Window/TextMeshPro/Font Asset Creator를 연다.
	4) Source Font File에서 다운받은 폰트를 선택하고, Generate Font Atlas 버튼을 누른다.
	5) 저장한다.
	6) 사용한다.
6. Aspect ratio: 스크린의 모양 (Width와 Height의 비율)
	Canvas (UI)의 비율이 된다.
	16:9 가 표준
7. Canvas/UI Scale Mode
	Constant Pixel Size: 픽셀 사이즈 유지
	Scale With Screen Size: 스크린사이즈에 맞게 스케일 조정 (항상 같은 크기를 유지한다.)
	Constant Physical Size: 물리적 크기를 유지 (현실세계의 자로 쟀을 때의 크기)
8. 9-Sliced image: 9개의 그리드를 만들고, 스트레칭 시 코너의 4개는 스트레칭 하지 않는다. [08.UI4-9-Sliced image 참고]
10. Button의 Inspector의 OnClick 이벤트 (Toggle의 OnValueChange 등)에서, Scene의 Object를 지정하고, 간단한 액션(GameObject.Active, InActive)을 바로 지정할 수 있다.
	지정한 Object에 달려있는 컴포넌트들에 기본적으로 존재하는 메소드들을 다 호출가능하니, 스크립트 쓰지말고 이걸로 잘 사용하자.
11. Digetic vs Non-diegetic
	World space UI: Digetic
	Screen space UI: Non-digetic (세상밖에 존재하는것이므로)
12. Canvas render mode
	Screen Space - Overlay: 기본
	Screen Space - Camera: 카메라의 일정거리 앞에 위치한다. Overlay와 비슷하나, 카메라의 영향을 받는다. (카메라의 셋팅등)
	World Space: 월드스페이스