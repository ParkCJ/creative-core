﻿==========================================
1. Shaders and Materials: 물체가 어떻게 보이는가를 만드는 것 (표면: 색, 반사, 물리 텍스쳐)
2. Lighting: 자연빛, 인공빛, 그림자
3. Animation: 물체에 움직임을 주는 것
4. Visual effects (VFX): 모션효과
5. Cameras: 너의 눈
6. Post-processing: 사진에 필터걸기
7. Audio
8. User interface (UI): 너와 상호작용
==========================================
VFX (Visual effects)
==========================================
1. 종류
	Environmental effects: 현실세계에 존재하는 물리적인 현상들
		예) 불, 비, 안개, 폭발 등

	Gameplay effects: 유저를 주목을 끌 목적으로 만든 효과들
		예) 물체 선택시 물체 주위에 빛나는 효과, 목표 달성시 축하 효과 등

2. VFX 아티스트의 역할: 사람이나 동물들을 뺀 움직이는 모든 CGI (Computer generated imagery)에 대한 책임
	예) 안움직이는 빌딩, 나무: 3D 아티스트, 3D 모델러의 역할
	예) 움직이는 캐릭터: 캐릭터 아티스트, 리깅 아티스트, 애니메이터의 역할
	예) 영화등에서, 실제로 셋트장에 존재할것 같은거를 뺀 모든 것이 VFX

3. 개발시스템
	Particle system: 컴포넌트 베이스
	VFX graph: 최신. 노드 베이스. 복잡함

4. Particle system: Object (파티클) 들의 모음.
	복잡한 VFX (캠프파이어 등): 여러개의 Particle system의 결합체 (불+연기+스파크)

	모듈: 파티클의 다른 분야를 컨트롤 하는 애 (예: 색 관련 모듈, 사이즈 관련 모듈 등)
		파티클 시스템은 모듈들로 만들어져있다.

		0) 공통
			값 지정 텍스트 박스 옆 '아래로 화살표' 클릭 -> Random between two constants: 두 값사이의 랜덤값으로 됨

		★기본 1) Main module: 가장 중요
			Duration: 파티클을 방출하는 기간 (초). Looping 을 할때는 한 사이클의 기간 (초)
				주의) 일단 방출된 애가 언제까지 살아있느냐는 관여하지 않는다.
			Start Lifetime: 파티클의 생명 기간 (초)
			Start Speed: 파티클의 속도
			Start Size: 파티클의 사이즈
			Max Particles: 화면에 동시에 존재 가능한 파티클의 최대수
			Looping: 반복실행 여부
			Play On Awake: 신에 추가될 때 실행할지 여부
			Prewarm: 게임 플레이와 동시에 두번째 사이클부터 표시 (사이클 한번을 돈 상태)
		2) Shape, Emission (배출)
			★기본 Shape
				Shape: 파티클이 생성되는 위치와 이동하는 방향을 모양으로 지정
					Cone: 방사향으로 퍼진다.
						Angle: 퍼지는 각도를 설정
						Radius: 크기를 설정
					Box: 정사각형 모양을 따른다.
				Scale: Shape의 크기를 설정
			★기본 Emission
				Rate over Time: 초당 생성되는 파티클의 수. 0이면 아무것도 생성하지 않는다.
					1이면 초당 1개 생성: Start하고 1초가 되는 순간 1개를 생성한다.
					2이면 초당 2개 생성: Start하고 0.5초가 되는 순간 1개를, 1초가 되는 순간 1개를 생성한다.
					10이면 초당 10개 생성: Start하고 0.1초가 되는 순간 1개를, 0.1초마다 1개씩 총 10개 생성

					주의) Start 와 동시에 바로 튀어나오게 하려면, Rate over Time 이 아닌, Burst를 써야한다.
					주의) Duration에 따른 예
						예1)
							Rate over Time: 1 => Start 하고 1초가 되는 순간 Emission을 한다.
							Duration: 1 => Start 하고 1초가 될때까지 Emission 한다.

							=> 1초가 된 시점에 Emission을 하려고 하나, Duration이 끝났으므로, 아무것도 Emission 되지 않는다.

						예2)
							Rate over Time: 1 => Start 하고 1초가 되는 순간 Emission을 한다.
							Duration: 3 => Start 하고 3초가 될때까지 Emission 한다.

							=> 1초가 된 시점과 2초가 된 시점에 Emission 되므로, 총 2번 Emission 한다.

				Bursts: 터트리기. 한번에 펑하고 생성된다.
					Time: 생성되는 시점
					Count: 파티클 갯수
					Cycles: 사이클
					Interval: 사이클사이의 인터벌 (초)
					Probability: 생성확률
		★기본 3) Renderer (파티클의 모양)
			Render mode: 렌더 모드를 설정
				Stretched Billboard: 길게 늘려서 렌더한다. (예: 정사각형 큐브->길쭉한 직사각형 큐브) 빠르게 움직이는 무언가를 표현할 때 좋다.
					Length Scale: 얼마나 길쭉한가
				Mesh: 큐브 등의 Mesh를 렌더한다.
			Material: 머티리얼을 설정한다.
			Render Alignment
				View: 파티클이 카메라를 바라보게 (정면으로) 한다.
				Local: 파티클의 Transform에 따른다.

		4) Color over Lifetime (색과 투명도를 설정한다.)
			위의 탭: 투명도
			아래의 탭: 색

		5) Texture Sheet Animation (Texture sheet를 사용가능하게 한다.)
			Texture sheet: 여러개의 이미지 (예: 4개의 이미지) 를 한장에 이미지에 넣어둔 것 (그리드형식. 예: 로우2, 컬럼2)

			방식1) 파티클의 이미지가 프레임마다 다음 이미지로 자동 변경된다. (4개 모두 사용될때까지)
				(1) Renderer.Material에서 Texture sheet를 사용하는 Material을 지정한다.
				(2) Tiles를 Texture sheet의 그리드에 맞춰 지정한다. (Row: 2, Col: 2)
				(3) StartFrame(사용되는 이미지의 인덱스)과 Cycles (모든 이미지가 사용되면 한바퀴)는 그대로 둔다. (0, 1)
				
			방식2) 파티클의 이미지가 4개중 하나로 랜덤으로 선택된다.
				(1) Renderer.Material에서 Texture sheet를 사용하는 Material을 지정한다.
				(2) Tiles를 Texture sheet의 그리드에 맞춰 지정한다. (Row: 2, Col: 2)
				(3) StartFrame을 Random between two constants로 변경하고, 0~3으로 지정한다.
				(4) Cycles를 0 (0.0001로 자동으로 바뀜)으로 지정한다.

		6) Size over Lifetime (크기를 설정한다.)
			예) 처음에는 Scale * 0 의 크기 -> Scale * 1 의 크기가 되어감
			Size
				커브: 커브로 크기를 설정한다. (커브를 선택한 경우, 인스펙터 제일 하단에 Particle System Curves가 나타남. X: 시간, Y: 크기)

		7) Noise (랜덤하게 만든다.)
			Strength
			Frequent

		8) Light (빛을 적용한다.)
			예) 반딧불
				Material 생성 (Lit으로)
				-> Point light object 생성 > Mode: Mixed > Shadow type: No shadows (성능을 위해) > Prefab으로 만듬
				-> 파티클의 Light에 위에서 만든 PointLight prefab을 설정
					Ratio: 1
					Random Distribution: 체크 해제
					Use Particle Color: 체크
					Size Affects Range: 체크 해제
					Alpha Affects Intensity: 체크
			예) 방출구에 적용: Light를 Prefab으로 만들지말고, Particle system의 자식으로 만든 후 Inactive > Lights의 Light에 적용

		9) Trails (지나간 흔적 만들기)
			예) 비행기가 날아가는 뒤로, 하얀 연기 만들기
				Renderer/Trail material: Default-Particle 적용
				Mode: Particles (Ribbon: 꼬리가 아니라 방출된 파티클들을 서로 연결하는 연결선을 만든다.)
				Ratio: 1
				Lifetime: 0.1 (Main module의 lifetime에 곱해짐)
				Minimum vertex distance: 0
				Die with Particles: 체크 (파티클이 사라질때 같이 사라짐)
				Texture Mode: Stretch (Trail의 모양)
				Size affects Width: 체크
				Width over Trail: 0.1 (Main module의 Start size에 곱해짐
				

	키로 잡을 것 4개
		Size: 파티클이 얼마나 큰지
		Shape: 파티클이 어떻게 퍼지는지
		Color: 파티클의 색이 어떻게 변하는지
		Timing
			Instant timing: 파티클이 생겨난후, 바로 사라지는가 (예: 총 쏠때 번쩍임)
				아래와 같이 설정한다.
				- Main/Speed: 0, Loop: false
				- Emission/Rate over time: 0, Burst count: 1
			Continues timing: 파티클이 생명주기가 끝날때까지, 혹은 플레이가 중단될때까지 계속 생겨나는가 (예: 토치의 불)

5. VFX graph
	장점
		1) 동시에 수백만개의 파티클 (<-> 파티클 시스템은 수천개. 예: 100만개 파티클의 기상효과에서 VFX graph는 100fps, Particle system은 5fps)
		2) 더욱 복잡한 상황을 만들 수 있음
	단점
		1) 초보자에게 어려움
		2) 하드웨어에 Strict (파티클 시스템과 비교해서)
		3) GPU를 사용하므로, 물리와 연동이 안됨 (물리는 CPU사용. Particle system은 CPU를 사용하므로 물리와 연동됨)

	어떤걸 선택할 것인가
		1) 개발 경험이 꽤 있고, 복잡하고, 수백만개의 파티클이 필요한 걸 만들때는 VFX graph
		2) 전문가가 아니고, 간단하고, 모든 디바이스에서 돌아가고, 물리랑 연동되는거 만들때는 Particle system

	노드는 블럭들로 구성되어 있음.
		
	1) 4개의 기본 노드 (Context nodes) [04.VFX1-VFX graph 참고]
		Spawn (파티클 만들기. Awake)
			- Emission 모듈에 대응

			[Spawn rate]				Rate over Time 에 해당
			[Burst]						Burst에 해당

		Initialize Particle (어디서, 어떻게 파티클이 처음 나타나는지. Start)
			- Main, Shape 모듈에 대응
			- Bounds: 렌더할 3D 공간. 카메라가 이 공간을 벗어나면 화면에 렌더되지 않는다. Inspector의 Preview에서 확인가능하다.

			[Set Size]					사이즈
			[Set lifetime]				생존 기간
			[Set velocity]				속도
			[Set position]				Shape에 해당

		Update Particle (각 프레임마다 연산. 각 파티클을 처리. Update)

			[Set size over lifetime]	크기를 변경한다.
			[Set color over lifetime]	색을 변경한다.
			[Gravity]					중력의 힘을 가한다.
			[Vector Field Force]		파티클에 힘을 가한다. (속도변화, 방향변화, 각도 변화등)
			[Turbulence]				파티클에 난기류의 힘을 가한다.
			[Set Scale.XYZ by Speed]
				정의: 스피드에 따라 파티클의 X, Y, Z스케일을 변경한다.
				목적: 스피드에 따라 크기가 달라지는 표현이 필요할 때
				예) 눈 (느리면 동그랗고, 빠르면 길쭉해짐)
					스피드가 0~80: 스케일=1
					스피드가 80~100: X스케일=0.5, Y스케일=1.5

		Output Particle Quad (각 파티클의 모양)
			- Renderer, Color over Lifetime, Size over Lifetime 모듈에 대응
			- Uv Mode: Default, Flipbook 등
			
			[Set Scale.xyz]				파티클의 크기를 결정 (길쭉 등)
			[Orient]					파티클이 어디를 향하고 있는지를 설정
			[Set Text Index Random]		Flipbook의 인덱스를 설정

	2) 그 외 노드들
		[Periodic Total Time]
			정의: 인터벌마다 새로운 값을 만든다. (범위안의 있는 값으로)
			목적: 바람세기 등, 시간마다 랜덤값이 필요한 경우 사용한다.

			Period: 인터벌
			Range: 값의 범위
			ex) Peroid: 2, Range: 0~1 => 2초마다 0~1사이의 랜덤값을 만든다.

		[Sine wave]
			정의: Min 값과 Max값 사이를 왔다갔다 하는 (물흐르듯이) 값을 만든다.

	3) 그 외
		Texture Sheet Animation 설정하기 (예: 4개의 이미지, 로우2, 컬럼2)

			방식1) 파티클의 이미지가 프레임마다 다음 이미지로 자동 변경된다. (4개 모두 사용될때까지)
				(1) Output Particle Quad / Main texture 를 사용하는 Material을 지정한다.
				(2) Output Particle Quad / UV mode 를 Flipbook 으로 설정
					Flipbook size를 x:2, y:2로 설정
				(3) Update particle 노드에 Flipbook player 블럭 생성 -> Frames rate (다음이미지로 초당 몇번 변경할 것인가) 설정
				
			방식2) 파티클의 이미지가 4개중 하나로 랜덤으로 선택된다.
				(1) Output Particle Quad / Main texture 를 사용하는 Material을 지정한다.
				(2) Output Particle Quad / UV mode 를 Flipbook 으로 설정
					Flipbook size를 x:2, y:2로 설정
				(3) Output Particle Quad 노드에 Set Text Index Random 블럭 생성 -> 0~3으로 지정해준다.

		BlackBoard: 프로퍼티 (인스펙터창에서 설정가능한)를 생성